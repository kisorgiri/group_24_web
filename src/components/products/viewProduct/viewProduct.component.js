import React, { Component } from 'react'
import httpClient from './../../../util/httpClient';
import notify from './../../../util/notify';
import { Loader } from './../../common/loader/loader.component';
import { Link } from 'react-router-dom';
import DateUtil from './../../../util/dateUtil';
import { connect } from 'react-redux';
import { fetchProduct_ac, removeProduct_ac } from './../../../actions/products/productAction';

const ImgURL = process.env.REACT_APP_IMG_URL;

class ViewProduct extends Component {

    constructor() {
        super();
    }

    componentDidMount() {
        console.log('check props now .>', this.props);
        if (this.props.incomingData) {
            return this.setState({
                products: this.props.incomingData
            })
        }
        this.fetchData();

    }

    fetchData(args) {
        this.props.fetch();
    }

    edit = (id) => {
        this.props.history.push(`/edit_product/${id}`);
    }

    remove(item, index) {
        // eslint-disable-next-line no-restricted-globals
        let confirmation = confirm("Are you sure to remove ?")
        if (confirmation) {
            this.props.removeItem(item, index);
        }
    }

    render() {
        console.log('check deleting item >>', this.props.deletingItem);
        console.log('check length >>', this.props.products.length);
        let content = this.props.isLoading
            ? <Loader />
            : <>
                <h2>View Products</h2>
                {this.props.incomingData && (
                    <button className="btn btn-success" onClick={this.props.resetSearch}> search again</button>
                )}
                <table className="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Brand</th>
                            <th>Category</th>
                            <th>Created Date</th>
                            <th>Created Time</th>
                            <th>Images</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {(this.props.products || []).map((item, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td> <Link to={`/product_details/${item._id}`}>{item.name}</Link></td>
                                <td>{item.brand}</td>
                                <td>{item.category}</td>
                                <td>{DateUtil.formatDate(item.createdAt)}</td>
                                <td>{DateUtil.formatTime(item.createdAt)}</td>
                                <td>
                                    <img src={`${ImgURL}/${item.images[0]}`} alt="product_image.jpg" width="200" />
                                </td>
                                <td>
                                    <button className="btn btn-info" onClick={() => this.edit(item._id)}>edit</button>
                                    <button className="btn btn-danger" onClick={() => this.remove(item, index)}>delete</button>
                                    {/* {item.$$isDeleting && (<span>show some loader</span>)} */}
                                    {this.props.deletingItem.indexOf(item._id) >= 0 && (<p>show loader</p>)}
                                </td>
                            </tr>
                        ))}

                    </tbody>
                </table>
            </>

        return (
            content
        )
    }
}

//what comes in
// the key defined here will be present in the props of the compoment
const mapStateToProps = store => ({
    products: store.product.products,
    isLoading: store.product.isLoading,
    deletingItem: store.product.deletingItem
})

// what goes out
const mapDispatchToProps = dispatch => ({
    fetch: () => dispatch(fetchProduct_ac()),
    removeItem: (item, index) => dispatch(removeProduct_ac(item, index))
})

// when connection props are defined form mapstatetoprops and mapdispatchtoProps
export default connect(mapStateToProps, mapDispatchToProps)(ViewProduct);

