import React from 'react';
import './sidebar.component.css';
import { Link } from 'react-router-dom';

export const SideBar = (props) => {

    return (
        <>
            <ul className="sidebar_menu">
                <li className="sidebar_item">
                    <Link to="/dashboard"> Home</Link>
                </li>
                <li className="sidebar_item">
                    <Link to="/add_product"> Add Product</Link>
                </li>
                <li className="sidebar_item">
                    <Link to="/view_product"> View Product</Link>
                </li>
                <li className="sidebar_item">
                    <Link to="/search_product"> Search Product</Link>
                </li>
                <li className="sidebar_item">
                    <Link to="/chat"> Messages</Link>
                </li>
            </ul>

        </>
    )
}

