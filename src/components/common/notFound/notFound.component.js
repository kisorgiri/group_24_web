import React from 'react';
export const NotFound = props => {
    return (
        <div>
            <p>Not Found</p>
            <img src="images/download.png" alt="404.jpg" width="600px"></img>
        </div>
    )
}