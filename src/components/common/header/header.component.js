import React from 'react';
import './header.component.css'
import { Link, withRouter } from 'react-router-dom';

const logout = (history) => {
    // clear localstorage and then redirect to login page
    localStorage.clear();
    history.push('/');
    // navigation
    // this will not have route props
}

const HeaderComponent = (props) => {
    let menu = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard/lkl">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/profile">Profile</Link>

            </li>
            <li className="logout">
                <button
                    className="btn btn-info"
                    onClick={() => logout(props.history)}
                >
                    Logout

                </button>

            </li>
        </ul>
        :
        <ul className="nav_list">
            <li className="nav_item">
                <Link to="/">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>

            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>

            </li>
        </ul>
    return (
        <>
            {menu}
        </>
    )
}

export const Header = withRouter(HeaderComponent);