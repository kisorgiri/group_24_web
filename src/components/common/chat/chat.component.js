import React, { Component } from 'react'
import * as io from 'socket.io-client';
import './chat.component.css';
import dateUtil from './../../../util/dateUtil';
import notify from '../../../util/notify';

export default class ChatComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                message: '',
                senderId: '',
                receiverId: '',
                time: '',
                senderName: '',
                receiverName: ''
            },
            messages: [],
            users: [],
            currentUser: null
        };
    }
    componentDidMount() {
        this.socket = io.connect(process.env.REACT_APP_SOCKET_URL);
        this.runSocket();
        let currentUser = JSON.parse(localStorage.getItem('user'));
        this.socket.emit('new-user', currentUser.username);

        this.setState({
            currentUser
        })
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))

    }
    handleSubmit = e => {
        e.preventDefault();
        const { data, users } = this.state;
        let senderId = users.find((item) => item.name === this.state.currentUser.username).id;
        if (!data.receiverId) {
            return notify.showInfo("Please Select a User to continue");
        }
        data.time = new Date();
        data.senderName = this.state.currentUser.username;
        data.senderId = senderId;
        this.socket.emit('new-msg', data);
        this.setState((pre) => ({
            data: {
                ...pre.data,
                message: ''
            }
        }));
    }

    runSocket() {
        this.socket.on('reply-msg', (messageData) => {
            const { messages, data } = this.state;
            messages.push(messageData);
            // swap sender as receiver
            data.receiverId = messageData.senderId;

            this.setState({
                messages,
                data
            });
        })

        this.socket.on('reply-msg-own', (messageData) => {
            const { messages } = this.state;
            messages.push(messageData);
            this.setState({
                messages
            });
        })

        this.socket.on('users', (users) => {
            this.setState({
                users
            })
        })

    }

    selectUser(selectedUserId) {
        this.setState((pre) => ({
            data: {
                ...pre.data,
                receiverId: selectedUserId
            }
        }))
    }
    render() {
        return (
            <>
                <h2>Let's Chat</h2>
                <ins>{this.state.currentUser && this.state.currentUser.username}</ins>
                <div className="row">
                    <div className="col-md-6">
                        <ins>Messages</ins>
                        <div className="chat_block">
                            <ul>
                                {this.state.messages.map((message, i) => (
                                    <li key={i}>
                                        <strong>{message.senderName}</strong>
                                        <p>{message.message}</p>
                                        <small>{dateUtil.relativeTime(message.time)}</small>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <form className="form-group" onSubmit={this.handleSubmit}>
                            <div className="row message_input">
                                <input className="form-control col-md-8" type="text" placeholder="your message here.." name="message" value={this.state.data.message} onChange={this.handleChange} ></input>
                                <button className="btn btn-success send" type="submit">send</button>

                            </div>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="chat_block">
                            <ul>
                                {this.state.users.map((user, index) => (
                                    <li key={index}>
                                        <button className="btn btn-default" onClick={() => this.selectUser(user.id)}>{user.name}</button>
                                    </li>
                                ))}
                            </ul>
                        </div>

                    </div>
                </div>
            </>
        )
    }
}
