import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Login } from './auth/login/login.component';
import { Register } from './auth/register/register.component';
import { Header } from './common/header/header.component';
import { SideBar } from './common/sidebar/sidebar.component';
import AddProduct from './products/addProduct/addProduct.component';
import ViewProduct from './products/viewProduct/viewProduct.component';
import { NotFound } from './common/notFound/notFound.component';
import { EditProduct } from './products/editProduct/editProduct.component';
import { ProductDetails } from './products/productDetails/productDetails.component';
import { SearchProduct } from './products/searchProduct/searchProduct.component';
import ForgotPassword from './auth/forgotPassword/forgotPassword.component';
import ResetPassword from './auth/resetPassword/resetPassword.component';
import ChatComponent from './common/chat/chat.component';

const Dashboard = props => {
    return (
        <div>
            <h2>Dashboard</h2>
            <p>Welcome to Our Store please use side navigation menu or contact system administrator for support-</p>
        </div>
    )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        localStorage.getItem('token')
            ? <div>
                <div className="nav_bar">
                    <Header isLoggedIn={true}></Header>
                </div>
                <div className="side_bar">
                    <SideBar></SideBar>
                </div>
                <div className="main">
                    <Component {...routeProps} />
                </div>
            </div>
            : <Redirect to="/"></Redirect>
    )}>
    </Route>
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        <div>
            <div className="nav_bar">
                <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
            </div>
            <div className="main">
                <Component {...routeProps} />
            </div>
        </div>
    )}>
    </Route>
}

const AuthRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(routeProps) => (
        !localStorage.getItem('token') // more valid way to identify logged in user
            ? <div>
                <div className="nav_bar">
                    <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
                </div>
                <div className="main">
                    <Component {...routeProps} />
                </div>
            </div>
            : <Redirect to='/dashboard' />
    )}>
    </Route>
}


const Routing = props => {
    return <Router>
        {/* <Footer isLoggedIn={true}></Footer> */}
        <Switch>
            <AuthRoute
                path="/"
                exact
                component={Login}
            />
            <AuthRoute
                path="/register"
                component={Register}
            />
            <AuthRoute
                path="/forgot_password"
                component={ForgotPassword}
            />
            <AuthRoute
                path="/reset_password/:id"
                component={ResetPassword}
            />
            <ProtectedRoute
                path="/dashboard"
                component={Dashboard}
            />
            <ProtectedRoute
                path="/add_product"
                component={AddProduct}
            />
            <ProtectedRoute
                path="/view_product"
                component={ViewProduct}
            />
            <ProtectedRoute
                path="/edit_product/:id"
                component={EditProduct}
            />
            <ProtectedRoute
                path="/product_details/:id"
                component={ProductDetails}
            />
            <ProtectedRoute
                path="/chat"
                component={ChatComponent}
            />
            <PublicRoute
                path="/search_product"
                component={SearchProduct}
            />
            <PublicRoute
                component={NotFound}
            />
        </Switch>
    </Router >
}

export default Routing;