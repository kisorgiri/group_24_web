import React from 'react';
import Routing from './../app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from './../../store';

export const App = (props) => {
    return <div>
        <Provider store={store}>
            <Routing />
        </Provider>
        <ToastContainer />
    </div>
}

// props 
// props are incoming data in a component

// state
// component's vitra ko data



// component
// component is basic building block for react js

// there are two ways of creating a component
// 1. functional component
// 2. class based component 

// there are two types of component
// 1 .stateful component
// 2 stateless componnet

// 1 functional component
// a function should start with capital letter
// it must return a html node
