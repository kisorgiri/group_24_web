import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Notify from './../../../util/notify';
import HttpClient from './../../../util/httpClient';

export default class ForgotPassword extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                email: ''
            },
            error: {
                email: ''
            },
            isSubmitting: false,
            isValidForm: false
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            // form validation
            this.validateForm(name);

        })
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'Required Field'
                break;
            default:
                break;

        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg

            }
        }), () => {
            const { error } = this.state;
            let err = Object
                .values(error)
                .filter(item => item)

            this.setState({
                isValidForm: err.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({ isSubmitting: true });
        HttpClient.POST('/auth/forgot_password', this.state.data)
            .then(data => {
                Notify.showSuccess("Password reset link sent to your email please check your inbox");
                this.props.history.push('/');
            })
            .catch(err => {
                Notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit">Submit</button>
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please provide your email address to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Email</label>
                    <input className="form-control" type="text" name="email" placeholder="Email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>
                    <br />
                    {btn}
                </form>
                <p>back to <Link to="/">login</Link></p>
            </>
        )
    }
}
