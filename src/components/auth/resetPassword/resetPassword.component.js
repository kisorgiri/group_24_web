import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Notify from './../../../util/notify';
import HttpClient from './../../../util/httpClient';

export default class ResetPassword extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                password: '',
                confirmPassword: ''
            },
            error: {
                password: '',
                confirmPassword: ''
            },
            isSubmitting: false,
            isValidForm: false
        };
    }

    componentDidMount() {
        this.userId = this.props.match.params['id'];
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            // form validation
            this.validateForm(name);

        })
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Required Field'
                break;

            default:
                break;

        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg

            }
        }), () => {
            const { error } = this.state;
            let err = Object
                .values(error)
                .filter(item => item)

            this.setState({
                isValidForm: err.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({ isSubmitting: true });
        HttpClient.POST('/auth/reset_password/' + this.userId, this.state.data)
            .then(data => {
                Notify.showSuccess("Password reset successfull, please login");
                this.props.history.push('/');
            })
            .catch(err => {
                Notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit">Submit</button>
        return (
            <>
                <h2>Reset Password</h2>
                <p>Please choose your new password</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input className="form-control" type="password" name="password" placeholder="Password" onChange={this.handleChange}></input>
                    <label> Confirm Password</label>
                    <input className="form-control" type="password" name="confirmPassword" placeholder="Confrim Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>
                    <br />
                    {btn}
                </form>
                <p>back to <Link to="/">login</Link></p>
            </>
        )
    }
}
