import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// Link can be used in any componet that are wrapped by Router(BrowserRouter)
import Notify from './../../../util/notify';
import HttpClient from './../../../util/httpClient';

export class Login extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                username: '',
                password: ''
            },
            error: {
                username: '',
                password: ''
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false
        };
        this.handleChanges = this.handleChanges.bind(this);
    }

    handleChanges(event) {
        const { name, value } = event.target;

        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg = this.state.data[fieldName]
            ? ''
            : `${fieldName} is required`
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            // check if form is valid
            let errors = Object
                .values(this.state.error)
                .filter(error => error);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // http call
        HttpClient.POST('/auth/login', this.state.data)
            .then(response => {
                console.log('response >>', response);
                Notify.showSuccess(`Welcome ${response.data.user.username}`);
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                this.props.history.push('/dashboard');
            })
            .catch(err => {
                Notify.handleError(err);
            })
    }

    render() {
        // try to implement applicaiton logic within a render block
        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">logingin...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" onClick={this.handleSubmit}>Login</button>

        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to start your session!</p>
                <form className="form-group">
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" placeholder="Username" id="username" name="username" onChange={this.handleChanges} />
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" id="password" onChange={this.handleChanges} />
                    <p className="error">{this.state.error.password}</p>
                    <br />
                    {btn}
                </form>
                <p>Don't have an account?</p>
                <p>Register <Link to="/register">here</Link>
                    <span className="float_right">
                        <Link to="/forgot_password">forgot password?</Link>
                    </span>
                </p>
            </div>
        )
    }
}
// props and state
// prevent default
// binding
// arrow notation function
// single html node
// setState