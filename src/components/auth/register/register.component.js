import React from 'react';
import { Link } from 'react-router-dom';
import Notify from './../../../util/notify';
import HttpClient from './../../../util/httpClient';

const DefaultForm = {
    name: '',
    email: '',
    phoneNumber: '',
    username: '',
    password: '',
    dob: '',
    gender: '',
}

export class Register extends React.Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...DefaultForm
            },
            error: {
                ...DefaultForm
            },
            isSubmitting: false,
            isValidForm: false
        };
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            // form validation
            this.validateForm(name);

        })
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'Requred Field'
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'Required Field'
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Weak Password'
                    : 'Required Field';
                break;
            default:
                break;

        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg

            }
        }), () => {
            //  now disable submit button when form is invalid
            const { error } = this.state;
            let err = Object
                .values(error)
                .filter(item => item)
            // .filter(function (item) {
            //     if (item) {
            //         return true;
            //     }
            // })

            this.setState({
                isValidForm: err.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({ isSubmitting: true });
        HttpClient.POST('/auth/register', this.state.data, { responseType: 'json' })
            .then(data => {
                Notify.showSuccess("Registration Successfull, Please Login");
                this.props.history.push('/');// navigate to login
            })
            .catch(err => {
                // if we get error 
                // we pass each and every error to error handling block
                Notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {

        let btn = this.state.isSubmitting
            ? <button disabled className="btn btn-info">submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit">Submit</button>

        return (
            <div>
                <h2>Register</h2>
                <p>Please Register to Continue</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Name</label>
                    <input className="form-control" type="text" placeholder="Name" name="name" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.name}</p>
                    <label>Email</label>
                    <input className="form-control" type="email" placeholder="Email" name="email" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>

                    <label>Phone Number</label>
                    <input className="form-control" type="number" placeholder="Phone Number" name="phoneNumber" onChange={this.handleChange}></input>
                    <label>Username</label>
                    <input className="form-control" type="text" placeholder="Username" name="username" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>

                    <label>Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Address</label>
                    <input className="form-control" type="text" placeholder="Address" name="address" onChange={this.handleChange}></input>
                    <label>Gender</label>
                    <input className="form-control" type="text" placeholder="Gender" name="gender" onChange={this.handleChange}></input>
                    <label>Date Of Birth</label>
                    <input className="form-control" type="date" name="dob" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                    <br />
                    <p>Already Registered? <Link to="/">back to login </Link></p>
                </form>
            </div>
        )
    }

}