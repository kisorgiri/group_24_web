import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components/app/app.component';
const container = document.getElementById('root');

// const Content = () => {
//     return <h2>I am Content</h2>
// }

ReactDOM.render(<App></App>, container);

// glossary
// component
// ===> class based component
// ====> functional component
// ====> stateful component
// ====== > stateless component
// props
// state
// setState
// 