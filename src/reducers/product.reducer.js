import { SET_IS_LOADING, SET_DELETING_ITEM, PRODUCT_RECEIVED, PRODUCT_DELETED } from './../actions/products/type';
const intitalState = {
    isLoading: false,
    products: [],
    deletingItem: []

}
// TODO
export const productReducer = (state = intitalState, action) => {
    switch (action.type) {
        case PRODUCT_RECEIVED:
            return {
                ...state,
                products: action.payload
            }
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case SET_DELETING_ITEM:
            return {
                ...state,
                deletingItem: [action.payload]
            }
        case PRODUCT_DELETED:
            const items = state.products;
            items.splice(action.payload, 1);
            return {
                ...state,
                products: items
            }

        default:
            return {
                ...state
            }
    }
}
// the sole purpose of reducer is to update store