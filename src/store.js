import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from './reducers';
import thunk from 'redux-thunk';

// in future we will add many middlewares
const middlewares = [thunk];
const initialState = {
    product: {
        products: [],
        deletingItem: [],
        isLoading: false
    }
};


export const store = createStore(rootReducer, initialState, applyMiddleware(...middlewares));
// function as an reducer
// initialState
// enhancer, middlewares
