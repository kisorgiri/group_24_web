import { SET_IS_LOADING, PRODUCT_RECEIVED, SET_DELETING_ITEM, PRODUCT_DELETED } from './type';
import httpClient from './../../util/httpClient';
import notify from './../../util/notify';

// function fetchProduct_ac(params) {
//     return function (dispatch) {
//         // calling a dispatch will execute reducer
//     }
// }

export const fetchProduct_ac = (params) => dispatch => {
    dispatch({
        type: SET_IS_LOADING,
        payload: true
    })
    httpClient.GET('/product', true)
        .then(response => {
            response.data.forEach(item => {
                item.$$isDeleting = false;
            })
            //   dispatch action to reducer
            dispatch({
                type: PRODUCT_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => notify.handleError(err))
        .finally(() => {
            dispatch({
                type: SET_IS_LOADING,
                payload: false
            })
        })
}

export function removeProduct_ac(item, index) {
    return function (dispatch) {
        dispatch({
            type: SET_DELETING_ITEM,
            payload: item._id
        })
        httpClient.DELETE(`/product/${item._id}`, true)
            .then(response => {
                notify.showInfo("Product Removed")
                dispatch({
                    type: PRODUCT_DELETED,
                    payload: index
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
    }
}