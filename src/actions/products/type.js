export const SET_IS_LOADING = 'SET_IS_LOADING'
export const SET_DELETING_ITEM = 'SET_DELETING_ITEM'
export const PRODUCT_RECEIVED = 'PRODUCT_RECEIVED'
export const PRODUCT_DELETED = 'PRODUCT_DELETED'